document.getElementById("closeModal").addEventListener('click', () => {
	document.getElementById("modal-container").classList.remove('show');
});

window.addEventListener("keyup", (e) => {
  if (e.keyCode === 27) {
    document.getElementById("modal-container").classList.remove('show');
  }
});
