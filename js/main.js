var cssRoot = document.querySelector(':root');
var CARDS = [
  ["<i class='bx bx-code-alt'></i>", '', 'Code Source', false, ["http://gitlab.com/KayrougeDev/dashboard", "Allez"]],
  ["<i class='bx bx-cog'></i>", '', 'Paramètre', true, 'settings']
]

function setCookie(cname, cvalue, exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    let expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }
  
function getCookie(cname) {
    let name = cname + "=";
    let ca = document.cookie.split(';');
    for(let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

var currentTheme = false;

if(getCookie('theme') != '') {
    if(getCookie('theme') == 'false') {
        var currentTheme = false;
    }
    else {
        var currentTheme = true;
    }
}

function getCSSVar(property) {
  var rs = getComputedStyle(cssRoot);
  return rs.getPropertyPriority('--'+property);
}

function setCSSVar(property,value) {
    cssRoot.style.setProperty("--"+property, value);
}

function setTheme(theme) {
    if(theme == true) {
        setCSSVar("text-color", '#fff');
        setCSSVar('link-back-color', '#fff');
        setCSSVar('link-text-color', '#000');
        setCSSVar('body-color', '#222');
        setCSSVar('card-color', 'rgba(42, 42, 42, 1)');
        setCSSVar('subtitle-text-color', 'rgba(255, 255, 255, 0.75)');
    }
    else if(theme == false) {
        setCSSVar("text-color", '#000');
        setCSSVar('link-back-color', '#000');
        setCSSVar('link-text-color', '#fff');
        setCSSVar('body-color', '#EAEAEA');
        setCSSVar('card-color', 'rgba(254, 254, 254, 1)');
        setCSSVar('subtitle-text-color', 'rgba(0, 0, 0, 0.75)');
    }
}

function redirect(url) {
  window.location.href = url;
}

function reloadCss()
{
    var links = document.getElementsByTagName("link");
    for (var cl in links)
    {
        var link = links[cl];
        if (link.rel === "stylesheet")
            link.href += "";
    }
}

function processCard(id) {
  document.querySelector('.modal').innerHTML = '';
  $(".modal").load("cardData/"+CARDS[id][4]+".html");
  document.getElementById("modal-container").classList.add('show');
}

// function resetDisabledCard() {
//   $("#dashboard-text").load("cardData/default.html");
// }

function init() {
  var cardHTML = '';

  console.log(CARDS);

  CARDS.forEach(card => {
    var isClickCard = '';
    console.log(card);
    if(card[3] == true) {
      isClickCard = "<a onclick='processCard("+CARDS.indexOf(card)+");'>Plus</a>";
    }
    else {
      if(card[4] != []) {
        var redirection = '"'+card[4][0]+'"';
        isClickCard = "<a onclick='redirect("+redirection+");'>"+card[4][1]+"</a>";
      }
    }
        
    cardHTML += '<div class="card"><div class="text"><h2>'+card[0]+'<span>'+card[1]+'</span></h2><p>'+card[2]+'</p>'+isClickCard+'</div></div>';
  });

  document.getElementById("container").innerHTML = cardHTML;
  setTheme(currentTheme);

  // $('html').on('click', function(e) {
  //   if (e.target !== this)
  //     return;
    
  //   resetDisabledCard();
  // });
}

init();

function changeTheme() {
    currentTheme = !currentTheme;
    setCookie("theme", currentTheme, 365);
    setTheme(currentTheme);
}
